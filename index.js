const axios = require('axios')
const express = require('express')
const app = express()
const port = 3000

function fmt(date) {
  if (String(date).length === 1) {
    return '0' + date
  }
  return date
}

function getDate() {
  var date = new Date();
  return date.getFullYear() + "-" + fmt(date.getMonth() + 1) + "-" + fmt(date.getDate());
}

app.get('/', (req, res) => {
  var ip = req.headers['x-forwarded-for'] || 
  req.socket.remoteAddress ||
  req.socket.remoteAddress;

  var date = getDate()

  ip = ip.replace(/^.*:/, '')
  res.json({
    "ip": ip,
    "time": date
    })
})

app.get('/hello', (req, res) => {
    res.send('Hello World!')
})

app.get('/goodbye', (req, res) => {
    res.send('Goodbye World!')
})

app.get('/ip/dayofweek', (req, res) => {
  dayofweek = axios({
    method: 'get',
    url: 'https://timeapi.io/api/Conversion/DayOfTheWeek/' + getDate()
  })
    .then((response) => {
      res.send(response.data.dayOfWeek)
    })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

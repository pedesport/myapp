Clone the repo and enter into the project folder:
```terminal
git clone https://gitlab.com/pedesport/myapp.git
cd myapp
```

Cofigure local gitlab account:
```terminal
git config --local user.name "Your Name"
git config --local user.email "Your Gitlab Email"
```

Checkout (change to) specific branch:
```terminal
git checkout dev
```

On Gitlab, create a new token: 
1. Go here: [https://gitlab.com/-/profile/personal_access_tokens](https://gitlab.com/-/profile/personal_access_tokens)
2. Set up a name for the token
3. Select all scopes
4. Click on "Create personal access token"
5. Copy the token in a secure place in your pc

Now you can change something on the project and commit and push the changes. For that,

1. See the files that have changed from the last commit:
    ```terminal
    git status
    ```
2. Add the files you want to commit with:
    ```terminal
    git add file1.txt file2.txt
    ```
3. Commit the changes using a descriptive message:
    ```terminal
    git commit -m "Descriptive commit message"
    ```
4. Push the commit to the remote:
    ```terminal
    git push origin dev
    ```